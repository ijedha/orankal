def m_to_e(m):
    result = m / 2.329
    return result
def readable_ečōki(e):
    if e >= 1000:
        return str(e / 1000) + " vāsečoki"
    if e >= 100:
        return str(e / 100) + " þūsečoki"
    if e >= 10:
        return str(e / 10) + " čāsečoki"
    if e >= 1:
        return str(e) + " ečōki"
    if e >= 0.1:
        return str(e / 0.1) + " jāršečoki"
    return str(e / 0.01) + " vjāšečoki"
    
def e_to_m(e):
    return e * 2.329
def readable_meter(m):
    if m >= 1000:
        return str(m / 1000) + " km"
    if m >= 1:
        return str(m) + " m"
    return str(m / 0.01) + " cm"
   
def cm_to_e(cm):
    return m_to_e(cm * 0.01)
def km_to_e(km):
    return m_to_e(km * 1000)
# done with that

