from math import floor, ceil
from convertdate import gregorian

MONTHS = [
    "Seðysa",
    "Ysto",
    "Šomi",
    "Þaša",
    "Koža",
]

WEEKDAYS = [
    "Ukeð",
    "Tersy",
    "Naki",
    "Sosaš",
    "Zeža",
    "Kasað",
    "Mešto",
    "Yntaja",
    "Ðesoðo",
    "Sasað",
    "Þareið",
    "Retað",
]

EPOCH = 2460025.5

def is_leap_year(year):
    return bool(floor(((year - 1) % 4 + 1) / 4) - floor(((year - 1) % 100 + 1) / 100))

def number_of_leap_days(year):
    if year <= 0: return 0
    return is_leap_year(year) + number_of_leap_days(year - 1)

def days_since_epoch(year):
    if year <= 0: return 0
    return 365 + is_leap_year(year) + days_since_epoch(year - 1)

def get_year(cdc):
    def counting(target, total, count):
        if total > target: return count - 1
        return counting(target, total + 365 + is_leap_year(count + 1), count + 1)
    return counting(cdc, 0, 0)

def to_jd(year, month, day):
    "Retrieve the Julian date equivalent for this date"
    return (day) + (month - 1) * 73 + days_since_epoch(year) + EPOCH - 1


def from_jd(jdc):
    "Create a new date from a Julian date."
    cdc = floor(floor(jdc) + .5 - EPOCH)

    year = get_year(cdc)

    yday = jdc - to_jd(year, 1, 1)

    month = floor(yday / 73) + 1
    day = yday - (month - 1) * 73 + 1

    return int(year), int(month), int(day)

def to_gregorian(year, month, day):
    return gregorian.from_jd(to_jd(year, month, day))

def from_gregorian(year, month, day):
    if year < 2023 or year == 2023 and month < 3 and day < 23:
        modifier = ceil((2023 - year) % 100)
        print(modifier)
        ryear, rmonth, rday = from_jd(gregorian.to_jd(year + modifier * 100, month, day))
        ryear -= modifier * 100
        return ryear, rmonth, rday
    return from_jd(gregorian.to_jd(year, month, day))

def date(year, month, day, cross=False):
    """
    If cross is True, replace month and day with '-' on leap day.
    """
    if cross and month == 6:
        return f"{str(year).rjust(3, '0')}/-/--"
    return f"{str(year).rjust(3, '0')}/{month}/{str(day).rjust(2, '0')}"

def dateText(year, month, day):
    if month == 6:
        return f"insert leap day name, {str(year).rjust(3, '0')}"
    if day == 73:
        weekday = "Woðos"
    else:
        weekday = WEEKDAYS[(day - 1) % 12]
    monthname = MONTHS[month - 1]
    return f"{weekday}, {str(day).rjust(2, '0')} {monthname}, {str(year).rjust(3, '0')}"

def print_array(array):
    arrdict = {'{}'.format(i): x for i,x in enumerate(array)}
    return '\n'.join('{}: {}'.format(key, val) for key, val in arrdict.items())

def get_months():
    return print_array(MONTHS)

def get_weekdays():
    return print_array(WEEKDAYS)

def date_from_gregorian(year, month, day):
    y, m, d = from_gregorian(year, month, day)
    return date(y, m, d)

def dateText_from_gregorian(year, month, day):
    y, m, d = from_gregorian(year, month, day)
    return dateText(y, m, d)
