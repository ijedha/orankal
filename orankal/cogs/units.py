import discord
import subprocess

from discord import app_commands
from discord.ext import commands
from util import unitconvert

class UnitCog(commands.Cog, name="Units (converter)"):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name="e2met")
    async def e2cm(self, ctx: discord.Interaction, e):
        """
        converts ečōki to a metric unit.
        """
        await ctx.send(unitconvert.readable_meter(unitconvert.e_to_m(float(e))))

    @commands.command(name="cm2e")
    async def cm2e(self, ctx: discord.Interaction, cm):
        """
        converts centimeters to ečōki.
        """
        await ctx.send(unitconvert.readable_ečōki(unitconvert.cm_to_e(float(cm))))

    @commands.command(name="m2e")
    async def m2e(self, ctx: discord.Interaction, m):
        """
        converts meters to ečōki
        """
        await ctx.send(unitconvert.readable_ečōki(unitconvert.m_to_e(float(m))))

    @commands.command(name="km2e")
    async def km2e(self, ctx: discord.Interaction, km):
        """
        converts kilometers to ečōki
        """
        await ctx.send(unitconvert.readable_ečōki(unitconvert.km_to_e(float(km))))    

async def setup(bot):
    await bot.add_cog(UnitCog(bot))
