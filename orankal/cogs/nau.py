#!/usr/bin/env python3

from __future__ import print_function

import discord
import subprocess
import sqlite3
import requests
import csv
import io

from dotenv import load_dotenv
from discord import app_commands
from discord.ext import commands
from math import ceil

class NauCog(commands.Cog, name="Nau"):
    def __init__(self, bot):
        self.bot = bot
        self.urls = {
            "ije": "https://docs.google.com/spreadsheets/d/1q8GwYPFhsZyeYijTrH-kTY-EqR25EHs5h-syjC2Y_YU/export?gid=1735706341&format=csv",
            "pr": "https://docs.google.com/spreadsheets/d/107n4ys3Sep_tqwvIZbtzBbrh64GS_TXcL1aS2hpRLRs/export?gid=722691806&format=csv"
        }
        
        self.lastSearch = []
        self.con = sqlite3.connect("data/db.sqlite3")
        self.cur = self.con.cursor()
        for key in self.urls:
            self.cur.execute(
                f"CREATE TABLE IF NOT EXISTS {key} (" + """
                    [id] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                    [part] TEXT NOT NULL,
                    [word] TEXT NOT NULL,
                    [transl] TEXT NOT NULL,
                    [desc] TEXT,
                    [etym] TEXT
                );
               """
            )

    @commands.command(name="pull")
    async def pull(self, ctx: discord.Interaction):
        """
        Refresh the list of words

        Downloads words from the google doc.
        """
        
        self.cur.execute("DELETE FROM sqlite_sequence;")
        
        for key, url in self.urls.items():
            await ctx.send(f"NAUDKJUZ: {key}")
            await ctx.send("Mysoþcun ukmo...")
            self.cur.execute(f"DELETE FROM {key};")
            await ctx.send("Lanen curyl...")
            data = requests.get(url).content.decode('utf-8')
            await ctx.send("Mysoþ sejez...")
            csvData = csv.reader(io.StringIO(data))

            await ctx.send("So lankal dy nauden cosaþ...")
            for row in csvData:
                row += [''] * (5 - len(row))
                if row[0] and row[1] and row[2]:
                    self.cur.execute(f"INSERT INTO {key} (part, word, transl, desc, etym) VALUES (?, ?, ?, ?, ?)", row)
                elif row[1]:
                    await ctx.send("Invalid row: " + row[1])
    
            self.con.commit()
        await ctx.send("Šersauka!")
        
    # Ijeða commands

    @commands.command(name="desc")
    async def desc(self, ctx: discord.Interaction, id):
        """
        Get full description of word from id number

        Usage: !desc <id>
        """
        self.cur.execute("SELECT * FROM ije WHERE id = ?", (id,))
        word = self.cur.fetchone()

        resultstring =  f"``` {str(word[0])} | {word[1]} : {word[2]} - {word[3]}"
        if word[4]:
            resultstring += f"{word[4]}"
        else:
            resultstring += "\nNo description provided."
        if word[5]:
            resultstring += f"\n---\n{word[5]}"
        resultstring += "```"

        await ctx.send(resultstring)

    @commands.command(name="ije")
    async def ije(self, ctx: discord.Interaction, *, arg):
        """
        Search the list of Ijeða words

        Usage: !ije <search query>
        """
        self.query = arg.lower()

        self.cur.execute("SELECT id, word, part, transl FROM ije WHERE word LIKE ? ORDER BY id", ('%' + arg.lower() + '%',))
        results = self.cur.fetchall()

        if len(results) == 0:
            await ctx.send(f"```No results for: {arg}```")
            return

        maxlength = 0
        for result in results:
            if len(result[1]) > maxlength:
                maxlength = len(result[1])

        self.lastSearch = []
        for result in results:
            self.lastSearch.append(f"{str(result[0]).rjust(3)} | {result[1].ljust(maxlength)} : {result[2].ljust(7)} - {result[3]}")

        await self.page(ctx, "1")

    @commands.command(name="eng")
    async def eng(self, ctx: discord.Interaction, *, arg):
        """
        Search the list of English words in the Ijeða dictionary.

        Usage: !eng <search query>
        """
        self.query = arg.lower()

        self.cur.execute("SELECT id, word, part, transl FROM ije WHERE transl LIKE ? ORDER BY id", ('%' + arg + '%',))
        results = self.cur.fetchall()

        if len(results) == 0:
            await ctx.send(f"```No results for: {arg}```")
            return

        maxlength = 0
        for result in results:
            if len(result[1]) > maxlength:
                maxlength = len(result[1])

        self.lastSearch = []
        for result in results:
            self.lastSearch.append(f"{str(result[0]).rjust(3)} | {result[1].ljust(maxlength)} : {result[2].ljust(7)} - {result[3]}")

        await self.page(ctx, "1")
        
    # Progāza commands
    
    @commands.command(name="descpr")
    async def descpr(self, ctx: discord.Interaction, id):
        """
        Get full description of word from id number

        Usage: !desc <id>
        """
        self.cur.execute("SELECT * FROM pr WHERE id = ?", (id,))
        word = self.cur.fetchone()

        resultstring =  f"``` {str(word[0])} | {word[1]} : {word[2]} - {word[3]}"
        if word[4]:
            resultstring += f"{word[4]}"
        else:
            resultstring += "\nNo description provided."
        if word[5]:
            resultstring += f"\n---\n{word[5]}"
        resultstring += "```"

        await ctx.send(resultstring)
        
    @commands.command(name="pr2eng")
    async def pr(self, ctx: discord.Interaction, *, arg):
        """
        Search the list of Progāza words

        Usage: !pr <search query>
        """
        self.query = arg.lower()

        self.cur.execute("SELECT id, word, part, transl FROM pr WHERE word LIKE ? ORDER BY id", ('%' + arg.lower() + '%',))
        results = self.cur.fetchall()

        if len(results) == 0:
            await ctx.send(f"```No results for: {arg}```")
            return

        maxlength = 0
        for result in results:
            if len(result[1]) > maxlength:
                maxlength = len(result[1])

        self.lastSearch = []
        for result in results:
            self.lastSearch.append(f"{str(result[0]).rjust(3)} | {result[1].ljust(maxlength)} : {result[2].ljust(7)} - {result[3]}")

        await self.page(ctx, "1")

    @commands.command(name="eng2pr")
    async def eng2pr(self, ctx: discord.Interaction, *, arg):
        """
        Search the list of English words in the Progāza dictionary.

        Usage: !eng <search query>
        """
        self.query = arg.lower()

        self.cur.execute("SELECT id, word, part, transl FROM pr WHERE transl LIKE ? ORDER BY id", ('%' + arg + '%',))
        results = self.cur.fetchall()

        if len(results) == 0:
            await ctx.send(f"```No results for: {arg}```")
            return

        maxlength = 0
        for result in results:
            if len(result[1]) > maxlength:
                maxlength = len(result[1])

        self.lastSearch = []
        for result in results:
            self.lastSearch.append(f"{str(result[0]).rjust(3)} | {result[1].ljust(maxlength)} : {result[2].ljust(7)} - {result[3]}")

        await self.page(ctx, "1")

    @commands.command(name="page")
    async def page(self, ctx: discord.Interaction, page):
        """
        See x page of search results

        Usage: !page <page number>
        """
        if page.isdigit():
            resultstring = f"```Results for: {self.query}\n"
            page = int(page)
            resultstring += '\n'.join(self.lastSearch[(page - 1) * 10 : (page - 1) * 10 + 10])
            resultstring += f"\n{(page - 1) * 10}-{(page - 1) * 10 + 10}/{len(self.lastSearch)}   page {page}/{ceil(len(self.lastSearch)/10)}"
            resultstring += "```"

            await ctx.send(resultstring)
            return

        await ctx.send("Invalid page or unknown error")

async def setup(bot):
    await bot.add_cog(NauCog(bot))
