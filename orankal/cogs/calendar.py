import discord
import convertdate
from datetime import datetime, date
import asyncio

from datetime import datetime, time, timedelta
from discord import app_commands
from discord.ext import commands, tasks

from util import roðai

class CalendarCog(commands.Cog, name="Roðai"):
    def __init__(self, bot):
        self.bot = bot
        self.waiting = True # waiting to send date to #date channel
        self.dateChannel = self.bot.get_channel(1226702406688182302)

        self.dailyDate.start()

    @commands.command(name="today")
    async def today(self, ctx):
        """
        Gets the current date, converts it to Roðai, and formats it nicely.

        Server (should be) in EST Timezone.
        """

        # datetime
        dt = datetime.now()
        # julian day
        jd = convertdate.julianday.from_datetime(dt)
        # gregorian date
        y, m, d = convertdate.gregorian.from_jd(jd)

        await ctx.send(
            roðai.dateText_from_gregorian(y, m, d)
        )

    @commands.command(name="date")
    async def date(self, ctx):
        """
        Gets the current date, and converts it to Roðai

        yyy/mm/dd format. Server (should be) in EST Timezone.
        """

        # datetime
        dt = datetime.now()
        # julian day
        jd = convertdate.julianday.from_datetime(dt)
        # gregorian date
        y, m, d = convertdate.gregorian.from_jd(jd)

        await ctx.send(
            roðai.date_from_gregorian(y, m, d)
        )

    @commands.command(name="roðai")
    async def roðai(self, ctx, y, m, d):
        """
        Converts date to Roðai

        Takes a Gregorian Calendar date, and returns the Roðai date as yyy/mm/dd.
        Usage: !roðai year month day
        ex. !rodai 2023 3 22
        returns: 000/1/01
        """

        # roðai dates
        ry, rm, rd = roðai.from_gregorian(int(y), int(m), int(d))

        await ctx.send(
           roðai.date(ry, rm, rd)
        )

    @commands.command(name="roðaiv")
    async def roðaiv(self, ctx, y, m, d):
        """
        Converts and formats date to Roðai

        Takes a Gregorian Calendar date, and returns the Roðai date formattd.
        Usage: !roðai year month day
        ex. !rodai 2023 3 22
        returns: 000/1/01
        """

        # roðai dates
        ry, rm, rd = roðai.from_gregorian(int(y), int(m), int(d))

        await ctx.send(
           roðai.dateText(ry, rm, rd)
        )

    @commands.command(name="greg")
    async def greg(self, ctx, y, m, d):
        """
        Converts from Roðai date

        Takes a Roðai date, and returns the Gregorian Calendar date as yyyy/mm/dd.
        Usage: !greg year month day
        ex. !rodai 0 1 1
        returns: 2023/3/22
        """

        # roðai dates
        gy, gm, gd = roðai.to_gregorian(int(y), int(m), int(d))

        await ctx.send(
            f"{gy}/{gm}/{gd}"
        )

    @commands.command(name="calendar")
    async def calendar(self, ctx, *, args):
        today = [date.today().year, date.today().month, date.today().day]
        if args:
            try:
                today = [int(x) for x in args.split(" ")]
            except ValueError:
                await ctx.send("can't understand :(")
                return
            if len(today) != 3:
                await ctx.send("can't understand :( format is 'yyyy mm dd'")
                return

        y, m, d = roðai.from_gregorian(*today)

        if m == 6:
            await ctx.send("Leap day, Year " + y)
            return

        calendar = [[[j * 12 + i + 1 == d, str(j * 12 + i + 1).rjust(2, "0")] for i in range(12)] for j in range(6)]
        grid = ("+------" * 12 + "+\n" + ("|      " * 12 + "|\n") * 3) * 6 + "+------" * 12 + "+"
        out = [list(x) for x in grid.split("\n")]

        today_tracker = False
        for ii, iv in enumerate(calendar):
            for ji, jv in enumerate(iv):
                if jv[0]:
                    out[ii * 4 + 1][ji * 7 + 2] = '/'
                    out[ii * 4 + 1][ji * 7 + 3] = '-'
                    out[ii * 4 + 1][ji * 7 + 4] = '-'
                    out[ii * 4 + 1][ji * 7 + 5] = '\\'

                    out[ii * 4 + 2][ji * 7 + 2] = '|'
                    out[ii * 4 + 2][ji * 7 + 5] = '|'
                    out[ii * 4 + 3][ji * 7 + 2] = '|'
                    out[ii * 4 + 3][ji * 7 + 5] = '|'

                    out[ii * 4 + 3][ji * 7 + 2] = '\\'
                    out[ii * 4 + 3][ji * 7 + 3] = '-'
                    out[ii * 4 + 3][ji * 7 + 4] = '-'
                    out[ii * 4 + 3][ji * 7 + 5] = '/'
                    today_tracker = True
                elif not today_tracker:
                    out[ii * 4 + 1][ji * 7 + 2] = '\\'
                    out[ii * 4 + 1][ji * 7 + 5] = '/'
                    out[ii * 4 + 3][ji * 7 + 2] = '/'
                    out[ii * 4 + 3][ji * 7 + 5] = '\\'

                out[ii * 4 + 2][ji * 7 + 3] = list(jv[1])[0]
                out[ii * 4 + 2][ji * 7 + 4] = list(jv[1])[1]

        labels = [i.center(6) for i in roðai.WEEKDAYS]
        out.insert(0, list('|' + ' '.join(labels) + '|'))
        out.insert(0, list('/' + '-' * 83 + '\\'))

        out.append(list('|' + ' ' * 13 + '|' + ' ' * 55 + '|' + ' ' * 13 + '|'))
        out.append(list('|' + str(y).center(13) + '|   ' + roðai.MONTHS[m].ljust(52) + '|' + "Woðos".center(13) + '|'))
        out.append(list('|' + ' ' * 13 + '|' + ' ' * 55 + '|' + ' ' * 13 + '|'))
        out.append(list('\\' + '-' * 83 + '/'))

        if d == 73:
            out[-4][-5] = '\\'
            out[-4][-6] = '-'
            out[-4][-7] = '-'
            out[-4][-8] = '-'
            out[-4][-9] = '-'
            out[-4][-10] = '-'
            out[-4][-11] = '/'

            out[-3][-5] = '|'
            out[-3][-11] = '|'

            out[-2][-5] = '/'
            out[-2][-6] = '-'
            out[-2][-7] = '-'
            out[-2][-8] = '-'
            out[-2][-9] = '-'
            out[-2][-10] = '-'
            out[-2][-11] = '\\'

        await ctx.send('```\n' + '\n'.join([''.join(x) for x in out]))

    @commands.command(name="minicalendar", aliases=["minical"])
    async def minical(self, ctx, *, args=None):
        today = [date.today().year, date.today().month, date.today().day]
        if args:
            try:
                today = [int(x) for x in args.split(" ")]
            except ValueError:
                await ctx.send("can't understand :(")
                return
            if len(today) != 3:
                await ctx.send("can't understand :( format is 'yyyy mm dd'")
                return
        y, m, d = roðai.from_gregorian(*today)
        if m == 6:
            await ctx.send("Leap day, Year " + y)
            return

        calendar = [[[1 + (j * 12 + i + 1 == d) + (j * 12 + i + 1 < d) * 2, str(j * 12 + i + 1).rjust(2, "0")] for j in range(6)] for i in range(12)]
        grid = []
        for i in calendar:
            row = []
            for j in i:
                if j[0] == 3:
                    row.append("\u001b[0;31m-" + j[1] + "-")
                if j[0] == 2:
                    row.append("\u001b[1;34m[" + j[1] + "]")
                if j[0] == 1:
                    row.append("\u001b[0m " + j[1] + " ")
            grid.append(row)

        out = []
        for i, v in enumerate(grid):
            out.append("\u001b[0;37m" + roðai.WEEKDAYS[i].rjust(6) + " |" + '\u001b[0;37m|'.join(v) + "\u001b[0;37m|")
        out.insert(0, roðai.date(y, m, d) + ' | ' + roðai.dateText(y, m, d))
        out.insert(1, "\u001b[0;37m------ /----" + "+----" * 5 + "\\")
        out.append("\u001b[0;37m------ \\----" + "+----" * 5 + "+\u001b[0m")
        if d == 73:
            out.append("\u001b[0;37m'" * (7 + 5*4) + "| \u001b[1;34m[Woðos]\u001b[0m |")
        else:
            out.append("\u001b[0;37m" + "'" * (7 + 5*4) + "|  Woðos  |")
        out.append("\u001b[0;37m" + "'" * (7 + 5*4) + "\\---------/")
        await ctx.send("```ansi\n" + '\n'.join(out) + "\n```")

    @tasks.loop(hours=24)
    async def dailyDate(self):
        # get seconds until midnight
        now = datetime.now()
        target = (now + timedelta(days=1)).replace(hour=0, minute=0, second=0, microsecond=0)
        diff = (target - now).total_seconds()
        await asyncio.sleep(diff)
        await self.minical(self.dateChannel)

async def setup(bot):
    await bot.add_cog(CalendarCog(bot))
