import discord
import subprocess

from discord import app_commands
from discord.ext import commands

class MiscCog(commands.Cog, name="Misc"):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name="ping")
    async def pong(self, ctx: discord.Interaction):
        await ctx.send("pong - bot")

async def setup(bot):
    await bot.add_cog(MiscCog(bot))
