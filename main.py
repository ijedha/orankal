#!/usr/bin/env python3

import os
import discord
from dotenv import load_dotenv
from discord.ext import commands

class Manager(object):
    def __init__(self):
        intents = discord.Intents(messages=True, guilds=True, members=True)
        intents.message_content = True

        self.workdir = os.path.dirname(os.path.realpath(__file__))

        self.bot = commands.Bot(command_prefix="!", intents=intents)
        self.bot.on_ready = self.on_ready
        self.bot.on_message = self.on_message

    async def on_ready(self):
        for filename in os.listdir('./cogs'):
            if filename.endswith('.py'):
                await self.bot.load_extension(f'cogs.{filename[:-3]}')
                print(f"did it the: {filename[:-3]}")
            else:
                print("ur an idiot")
        print("done")

    def run(self):
        load_dotenv()
        self.bot.run(str(os.getenv("BOT_TOKEN")))

    async def on_message(self, ctx):
        await self.bot.process_commands(ctx)

if __name__ == "__main__":
    bot = Manager()
    bot.run()
